package com.invicara;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.google.common.io.Files;
import com.invicara.exception.WeatherForcastException;
import com.invicara.service.WeatherService;




@RunWith(SpringRunner.class)
@SpringBootTest()

public class WeatherApplicationTests {
	
    private static final Logger logger = LoggerFactory.getLogger(WeatherApplicationTests.class);
    
	private static final int PORT=9010;	
    private static final String RELATIVE_URL = "/forcast?city=chennai";   
    private static final String TEST_SERVER_URL="http://localhost:7777/forcast?city=chennai"; 
    private static final String SERVER="http://localhost"; 
   

    @Autowired
    private WeatherService service ;     
    private RestTemplate  template;
    private String forcastDetails;
    private String expectedWeatherDetails;
    
    @Rule
    public WireMockRule mWireMockRule = new WireMockRule(PORT); 
    
    @Before
    public void setUp() throws IOException {    	
    	  template = new RestTemplateBuilder().build();     	  
    	  File forcastSJSON= new File(this.getClass().getResource("/stub/forcast.json").getFile());    	  
    	  forcastDetails=new String(Files.toString(forcastSJSON, StandardCharsets.UTF_8));
    	  logger.info("Forcast Details :{}",forcastDetails);    	  
    	  File expectedWeatherSJSON= new File(this.getClass().getResource("/stub/expected-forcast.json").getFile());    	  
    	  expectedWeatherDetails=new String(Files.toString(expectedWeatherSJSON, StandardCharsets.UTF_8));
    	  logger.info("Exepected Forcast Details :{}",expectedWeatherDetails);    	  

    }
    
    @Test
	public void testCityWeatherForcast() throws WeatherForcastException, Exception {    	
    	stubFor(get(urlEqualTo(RELATIVE_URL)).willReturn(aResponse().withHeader("Content-Type", "application/json").withStatus(HttpStatus.OK.value())
       .withBody(forcastDetails)));  				
  		ResponseEntity<String> response= template.getForEntity(SERVER+":"+PORT+RELATIVE_URL, String.class);  
  		assertTrue("successfully received city forcast details",response.getStatusCode()==HttpStatus.OK);
  		assertNotNull(response);    		
  		ResponseEntity<List<Map<String,Object>>> filteredResponse =WeatherService.filterJSONResponse(response.getBody());  		
  		String weatherDetails = new ObjectMapper().writeValueAsString(filteredResponse.getBody());  		
  		logger.info("Filtered Response ::{}" ,weatherDetails);  		
  		assertTrue("filtered city forcast details", expectedWeatherDetails.trim().equalsIgnoreCase(weatherDetails.trim()));  	
  		
		
	}
    
	
}
