package com.invicara.fanalyser;

import org.springframework.boot.diagnostics.AbstractFailureAnalyzer;
import org.springframework.boot.diagnostics.FailureAnalysis;

import com.invicara.exception.InvalidAppIdException;

/**
 * @author psusaina
 *
 */
public class InvalidAppidFailureAnalyser extends AbstractFailureAnalyzer<InvalidAppIdException>{
		@Override
	protected FailureAnalysis analyze(Throwable rootFailure, InvalidAppIdException cause) {		
		return new FailureAnalysis("Could not start up service","Provide valid service provide Application ID" +cause.getAppid(),rootFailure);
	}

}
