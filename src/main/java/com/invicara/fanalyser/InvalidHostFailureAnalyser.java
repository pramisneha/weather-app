package com.invicara.fanalyser;

import org.springframework.boot.diagnostics.AbstractFailureAnalyzer;
import org.springframework.boot.diagnostics.FailureAnalysis;


import com.invicara.exception.InvalidHostException;

/**
 * @author psusaina
 *
 */
public class InvalidHostFailureAnalyser extends AbstractFailureAnalyzer<InvalidHostException>{

	@Override
	protected FailureAnalysis analyze(Throwable rootFailure, InvalidHostException cause) {		
		return new FailureAnalysis("Could not start up service","Provide valid service provider host details" +cause.getHost(),rootFailure);
	}

}
