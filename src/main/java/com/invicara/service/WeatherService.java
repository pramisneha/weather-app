package com.invicara.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.invicara.exception.WeatherForcastException;

/**
 * @author psusaina
 * Weather Service API
 */
public interface WeatherService {	
	
	ResponseEntity<List<Map<String, Object>>> getCityWeather(String cityName) throws  WeatherForcastException;	
	
	public static String go() {
		return "Hello World";
	}
	enum columnNames{wind,dt,main};
	
	/**
	 * Filter response based on required columns
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	public static ResponseEntity<List<Map<String,Object>>> filterJSONResponse(String response) throws IOException{
		
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String, List<Map<String, Object>>> newMap = new HashMap<>();
		newMap = objectMapper.readValue(response, Map.class);
		List<Map<String, Object>> list = newMap.get("list");
	
		List<Map<String, Object>> filteredList  = list.stream()
				.map(map -> map.entrySet().stream()
						.filter(m -> (m.getKey().equalsIgnoreCase(columnNames.wind.toString()) || m.getKey().equalsIgnoreCase(columnNames.dt.toString()))
								|| m.getKey().equalsIgnoreCase(columnNames.main.toString()))
						.collect(Collectors.toMap(m -> m.getKey(), m -> m.getValue())))
				.collect(Collectors.toList());
				
		return ResponseEntity.status(HttpStatus.OK).body(filteredList);
		
	}
}
