package com.invicara.service;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import com.invicara.config.ApplicationProperties;
import com.invicara.config.RestTemplateConfiguration;
import com.invicara.exception.CityNotFoundException;
import com.invicara.exception.InvalidAppIdException;
import com.invicara.exception.InvalidHostException;
import com.invicara.exception.WeatherForcastException;
import org.springframework.http.ResponseEntity;

/**
 * @author psusaina
 * Weather Forcast Service
 */
@Component
@EnableConfigurationProperties(ApplicationProperties.class)
public class WeatherForcastServie implements WeatherService {
	
	private final Logger logger = LoggerFactory.getLogger(WeatherForcastServie.class);
	private RestTemplateConfiguration restTemplateService;	
	private ApplicationProperties appProperties;
		
	
	/**
	 * Get weather forcast for a city
	 */
	public ResponseEntity<List<Map<String, Object>>> getCityWeather(String cityName) throws CityNotFoundException ,WeatherForcastException{

		ResponseEntity<String> responseEntity = null;
		String response = null;
		ResponseEntity<List<Map<String, Object>>> filteredResponse = null;

		try {

			String queryURI = new StringBuilder(appProperties.getHost()).append(appProperties.getQueryurl()).toString();
			logger.info("Query URI ::{}", queryURI);
			Map<String, String> params = new HashMap<String, String>();
			params.put("q", cityName);
			params.put("appid", appProperties.getAppid());
			responseEntity = restTemplateService.invokeRestService(queryURI, params, HttpMethod.GET);
			response = responseEntity.getBody();
			logger.info("Status  Code :{}", responseEntity.getStatusCodeValue());
			filteredResponse = WeatherService.filterJSONResponse(response);

		} catch (HttpClientErrorException hce) {
			logger.error("client error excpetion");
			if (hce.getStatusCode().is4xxClientError())
				throw new CityNotFoundException(hce.getMessage());

		} catch (Exception ex) {
			logger.error(" Exception has occured" + ex.getMessage());
			throw new WeatherForcastException(ex.getMessage());
		}

		return filteredResponse;
	}

	@Autowired
	public void setRestTemplateService(RestTemplateConfiguration restTemplateService) {
		this.restTemplateService = restTemplateService;
	}	
	
	public WeatherForcastServie(ApplicationProperties appProperties) {		
		
		this.appProperties=appProperties;		
		if (StringUtils.isEmpty(this.appProperties.getHost())) {
			throw new InvalidHostException(appProperties.getHost());
		}		
		if (StringUtils.isEmpty(this.appProperties.getAppid())) {
			throw new InvalidAppIdException(appProperties.getAppid());
		}
	}	
}
