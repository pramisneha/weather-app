package com.invicara.controller;


import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.invicara.exception.WeatherForcastException;
import com.invicara.service.WeatherService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;



/**
 * @author psusaina
 * Weather Forcast API
 */
@RestController
public class WeatherController {
	
	private final Logger logger = LoggerFactory.getLogger(WeatherController.class);		
	private final WeatherService service ;			
	
	@GetMapping(value="/forcast", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.FOUND)
	@ApiOperation(value = "weather forcast details", notes = "read weather forcast details for a city ", response = List.class)
	public ResponseEntity<List<Map<String, Object>>> getWeatherForcast( @ApiParam(value = "city") @RequestParam("city") String city) throws IllegalArgumentException,WeatherForcastException{	
		
		logger.info("Retrieving weather forcast for the city :{}",city);	
		if(StringUtils.isEmpty(city)) {			
			throw new IllegalArgumentException();			
		}			
		return service.getCityWeather(city);
	}
		
	
	public WeatherController(WeatherService service) {		
		this.service = service;
	}
}
