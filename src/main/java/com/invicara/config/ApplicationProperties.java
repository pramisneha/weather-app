package com.invicara.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author psusaina
 * Application properties are required to start weather app service
 * host is service provider host ,appid is application id to access the service
 */

@ConfigurationProperties("weather")
public class ApplicationProperties {	
	
	private String appid= "0049058f942c6a19bfa550036cbcb840";
	private String host="https://api.openweathermap.org";
	private String queryurl="/data/2.5/forecast";
	
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getQueryurl() {
		return queryurl;
	}
	public void setQueryurl(String queryurl) {
		this.queryurl = queryurl;
	}
	
}
