package com.invicara.config;

import java.util.Arrays;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


/**
 * @author psusaina
 * Rest template configuration to consume third party services
 */
@Component
public class RestTemplateConfiguration {	
	
	private final Logger logger = LoggerFactory.getLogger(RestTemplateConfiguration.class);	
	private HttpHeaders headers;	
	private RestTemplate restTemplate;
	
	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public RestTemplateConfiguration(){		
		restTemplate= new RestTemplateBuilder().build();
		
	}
			
	/**
	 * Consume Rest service using Rest template
	 * @param headers
	 * @param relUrl
	 * @param params
	 * @param method
	 * @return
	 */
	public ResponseEntity<String> invokeRestService(String relUrl, Map<String, String> params, HttpMethod method) {	
				
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(relUrl);
		//RestTemplate restTemplate= new RestTemplateBuilder().build();
		
		if (params != null && !params.isEmpty()) {
			for (Map.Entry<String, String> param : params.entrySet()) {
				builder.queryParam(param.getKey(), param.getValue());
			}
		}
		
		String uri = builder != null ? builder.toUriString() : null;
		logger.info("URI :{}",uri);
		return restTemplate.exchange(uri, method, null, String.class);
		
	}	

	/**
	 * Initializing http headers
	 */
	@PostConstruct
	public void initHttpHeaders() {		
		logger.info("Initializing REST Templeate with HTTP headers");
		headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));        
	}
	
}
