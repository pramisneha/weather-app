package com.invicara.exception;

import java.util.HashMap;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author psusaina
 * Exception Handler to handle all exception
 */
@Controller
@ControllerAdvice
public class ForcastExceptionHandler extends ResponseEntityExceptionHandler{
	
		
	/**
	 * Exception is thrown when city is not available
	 * @param ex
	 * @param request 
	 * @return
	 */
	@ExceptionHandler(CityNotFoundException.class)
	public final ResponseEntity<Map<String,String>> handleCityNotFoundException(CityNotFoundException ex,WebRequest request) {
				
		logger.error("city not found exception has occured");		
		Map<String,String> errorMap = new HashMap();		
		errorMap.put("Message", "city is not found");
		errorMap.put("Error", ex.getLocalizedMessage());		
		return new ResponseEntity<Map<String,String>>(errorMap, HttpStatus.NOT_FOUND);
	}
	
	/**
	 * Exception is thrown when city value is empty or null
	 * @param ex
	 * @param request 
	 * @return
	 */	
	@ExceptionHandler(IllegalArgumentException.class)
	public final ResponseEntity<Map<String,String>> handleIllegalArgumentException(IllegalArgumentException ex,WebRequest request) {				
		
		logger.error("illegal Argument exception has occured");		
		Map<String,String> errorMap = new HashMap();		
		errorMap.put("Message", "Nothing to geocode");
		errorMap.put("Error", ex.getLocalizedMessage());
		
		return new ResponseEntity<Map<String,String>>(errorMap, HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * Handlers all other Exceptions
	 * @param ex
	 * @param request 
	 * @return
	 */			
	@ExceptionHandler(WeatherForcastException.class)
	public final ResponseEntity<Map<String,String>> handleExceptions(WeatherForcastException ex,WebRequest request) {				
		
		logger.error("Exception has occured");		
		Map<String,String> errorMap = new HashMap();		
		errorMap.put("Message", "Internal server error! Server is not reachable");
		errorMap.put("Error", ex.getLocalizedMessage());		
		return new ResponseEntity<Map<String,String>>(errorMap, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
