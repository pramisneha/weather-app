package com.invicara.exception;

/**
 * @author psusaina
 * Handle all other exceptions
 */
public class WeatherForcastException extends Exception {
	
	private static final long serialVersionUID = -6495839373265662849L;

	public  WeatherForcastException(String message) {
		super(message);
	}

}
