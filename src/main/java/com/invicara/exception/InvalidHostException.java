package com.invicara.exception;

/**
 * @author psusaina
 * Handle host is missing or mull
 */
public class InvalidHostException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	private final String host;

	public InvalidHostException(String host) {
		super(String.format("Invalid host details '%s'", host));
		this.host = host;
	}

	public String getHost() {
		return host;
	}
}
