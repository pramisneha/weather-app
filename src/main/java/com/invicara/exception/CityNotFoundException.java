package com.invicara.exception;


/**
 * @author psusaina
 * City is not found  Exception
 */
public class CityNotFoundException extends RuntimeException {	

	private static final long serialVersionUID = 1L;

	public CityNotFoundException(String message) {
		super(message);
		System.out.println("CityNotFoundException");
		
	}
}
