package com.invicara.exception;

/**
 * @author psusaina
 * Handle Appid is missing or null
 */
public class InvalidAppIdException  extends RuntimeException{
	
	private static final long serialVersionUID = 1L;
	private final String appId;

	public InvalidAppIdException(String appId) {
		super(String.format("Invalid AppID '%s'", appId));
		this.appId = appId;
	}

	public String getAppid() {
		return appId;
	}
}
